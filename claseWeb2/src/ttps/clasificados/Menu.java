package ttps.clasificados;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Menu
 */
@WebServlet("/Menu")
public class Menu extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ServletContext sc;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Menu() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
		sc = config.getServletContext();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	
	// Seg�n el el perfil de usuario genera un men� distinto.
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	

		response.setContentType("text/html");
		Usuario usuarioLogueado = (Usuario) request.getAttribute("objetoUsuario");
		String perfilUser;
		String nombreUser;
		perfilUser = usuarioLogueado.getPerfil();
		nombreUser = usuarioLogueado.getUsuario();
		PrintWriter out = response.getWriter();
		out.println("<html><body>");
		if (perfilUser == "Administrador") {
			out.println("<h1> Hola! Administrador opc1Admin opc2Admin  </h1>");

		}else {
			out.println("<h1> Hola Publicador opc1Pub opc2Pub  </h1>");
		}
		
	//	ServletContext sc=this.getServletContext();
		SitiosClasficiados sitio = (SitiosClasficiados) sc.getAttribute("unSitioClasificado");
		out.print(" <footer>" + sitio.getEmail()  + "</footer>");
		out.print(" </body></html>");
		out.close();





	}

}
