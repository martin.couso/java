<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="estilos/estiloPrincipal.css">
<script type="text/javascript" src="scripts/animacionForms.js"></script>
<title></title>
</head>
<body>
	<div id="divRegistro" style="height: auto;">
		<form id="msform">

			<table class="divTablaComentarios">
				<tr>
					<th>Nombre Evento</th>
					<th>Ubicacion</th>
					<th>Fecha</th>
					<th>Horario</th>
					<th>Temas</th>

					<th>Modificar</th>
					<th>Eliminar</th>

				</tr>
				<tr>
					<td>Robotica</td>
					<td>50 y 120</td>
					<td>28/10/2014</td>
					<td>16:00</td>
					<td>Aprender a programar Robots</td>
					<td><a href="#verEditarEvento"><img
							src="imagenes/editar.jpg" height="25" width="25"></a></td>
					<td><img src="imagenes/delete_icon.png" height="25" width="25"></td>
				</tr>

			</table>

		</form>
	</div>
	<div id="verEditarEvento" class="modalDialog">
		<div>
			<a href="#close" title="Close" class="close">X</a>
			<h2>Editar Evento</h2>
			<form id="msform">



				<input type="text" name="text" value="Robotica"
					placeholder="Nombre Evento" /> <input type="text" name="ubicacion"
					value="50 y 120" placeholder="Ubicacion" /> <input type="text"
					name="fecha" value="28/10/2014" placeholder="Fecha DD/MM/AAAA" />
				<input type="text" value="16:00" name="horario"
					placeholder="Horario" />


				<textarea name="temas" value="Aprender a programar Robots"
					placeholder="Temas del evento"></textarea>
				<input type="submit" name="submit" class="submit action-button"
					value="Registrar Evento!" />

			</form>
		</div>
	</div>

</body>
</html>