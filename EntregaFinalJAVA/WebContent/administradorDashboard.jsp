<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="estilos/estiloPrincipal.css" />
<script type="text/javascript" src="scripts/scriptMenuLateral.js"></script>
<script type="text/javascript" src="scripts/scriptCambiarDiv.js"></script>
<script type="text/javascript" src="scripts/animacionForms.js"></script>
<link rel="shortcut icon" href="favicon.ico" />
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<title>Escritorio de Administraci�n</title>
</head>
<body>
	<%@include file="header.jsp"%>

	<div class="escritorioAdministracion">
		<div id="wrapper">

			<ul class="menu">
				<li class="item1"><a href="#">ABM Tablas</a>
					<ul>
						<li class="subitem1"><a href="javascript:void(0);"
							id="botonAltaUsuarios" onclick="getDiv(1)">Alta de Usuarios</a></li>
						<li class="subitem2"><a href="javascript:void(0);"
							id="botonAltaUsuarios" onclick="getDivListarUsuarios(1)">Listar,Modificar,Eliminar
								Usuarios </a></li>

						<li class="subitem2"><a href="#">Listar,Modificar,Eliminar
								Roles</a></li>
						<li class="subitem1"><a href="javascript:void(0);"
							id="botonAltaUsuarios" onclick="getDivAltaRoles(1)">Alta de
								Roles</a></li>
					</ul></li>
				<li class="item2"><a href="#">Eventos Academicos</a>
					<ul>
						<li class="subitem1"><a href="javascript:void(0);"
							id="botonAltaEvento"
							onclick="getDivAltaEvento(1)">Alta
								de Eventos</a></li>
						<li class="subitem2"><a href="javascript:void(0);"
							id="botonAltaEvento"
							onclick="getDivListarEventos(1)">Listar,Modificar,Eliminar
								Eventos </a></li>
					</ul></li>
				<li class="item3"><a href="#">Manejo de Viajeros </a>
					<ul>

						<li class="subitem3"><a href="javascript:void(0);"
							id="botonAltaUsuarios" onclick="getDivBloqueoUsuarios(1)"
							Bloquear Viajeros>Bloqueo Viajes Usuarios</a></li>
					</ul></li>
				<li class="item4"><a href="#">Mensajes </a>
					<ul>
						<li class="subitem1"><a href="javascript:void(0);"
							id="botonAltaUsuarios" onclick="getDivEnviarMensaje(1)">
								Enviar Mensaje</a></li>
						<li class="subitem1"><a href="#">Mensajes Enviados</a></li>
						<li class="subitem2"><a href="#">Mensajes Recibidos</a></li>
						<li class="subitem3"><a href="#">Mensajes Eliminados</a></li>

					</ul></li>
			</ul>

		</div>
		<span></span>
		<div class="areaTrabajo">

			<div id="divQueCambia"></div>



		</div>
	</div>
	<script src="scripts/scriptMenuLateral.js"></script>
	<script src="scrits/scriptCambiarDiv.js"></script>
	<script src="scripts/animacionForms.js"></script>


</body>
</html>