<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="estilos/estiloPrincipal.css">
<script type="text/javascript" src="estilos/prefixfree.min.js"></script>
<script type="text/javascript" src="scripts/scriptSeccionVertical.js"></script>
<script type="text/javascript" src="scripts/bjqs.min.js"></script>
<link rel="shortcut icon" href="favicon.ico" />
<title>Inicio - InfoP@@L</title>
</head>
<body>
	
	
	<section id="s1">
		<%@include file="header.jsp"%>
		<div class="pin">
			<img alt="logo" src="imagenes/logo_index.png">
		</div>
		<!--  <div class="dot"></div>*/-->
		<footer class="barraDivisora">
			<p>
				<a href="#s2">�Conocenos mejor! Click aqu� </a>
			</p>

		</footer>
	</section>

	<!-- Importamos libreria jQuery -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
	<!-- Plugin para dar mas efecto a nuestro scroll -->
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
	<!-- Importamos nuestro script -->
	<script src="scripts/scriptSeccionVertical.js"></script>

	<section id="s2">





<div id="openModal" class="modalDialog">
	<div>
		<a href="#close" title="Close" class="close">X</a>
		<h2>Agregar un nuevo recorrido</h2>
		<p>esta funcionalidad debe permitir establecer claramente
cu�l ser� el recorrido que se dar� de alta. El objetivo es el de ofrecerlo para que otros
pasajeros se sumen a �l mediante un acuerdo.</p>
	<img src="imagenes/funcion1_seccion2.png"	>
	</div>
</div>


		<div class="bodySeccion2">
			
			
			
			<h1 class="h1Seccion2">Algunos de nuestros servicios</h1>
			<ul class="grid">
				<li  class="one"><a href="#openModal"></a><span class="s"></span></li>
				<li class="two"><span class="s"></span></li>
				<li class="one"><span class="s"></span></li>
				<li class="two"><span class="s"></span></li>
			</ul>
			
		</div>
		
	</section>

<%@include file="footer.jsp"%>
</body>
</html>