<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="estilos/estiloPrincipal.css">
<script type="text/javascript" src="scripts/animacionForms.js"></script>
<title>Insert title here</title>
</head>
<body>
	<div id="divRegistro" style="height: auto;">
		<form id="msform">
			<table class="divTablaComentarios">
				<tr>
					<td><strong>Nombre</strong></td>
					<td><strong>Apellido</strong></td>
					<td><strong>Usuario</strong></td>
					<td><strong>Telefono</strong></td>
					<td><strong>Direccion</strong></td>
					<td><strong>Email</strong></td>
					<td><strong>Modificar</strong></td>
					<td><strong>Eliminar</strong></td>
				</tr>
				<tr>
					<td>Lisandro</td>
					<td>Di Renta</td>
					<td>lisandrodirenta</td>
					<td>4864444</td>
					<td>7 y 50</td>
					<td>lisandrodirenta@gmail.com</td>
					<td><img src="imagenes/editar.jpg" height="25" width="25"></td>
					<td><img src="imagenes/delete_icon.png" height="25" width="25"></td>

				</tr>
				<tr>
					<td>Martin</td>
					<td>Couso</td>
					<td>martincouso</td>
					<td>4554444</td>
					<td>6 y 60</td>
					<td>martin.couso@gmail.com</td>
					<td><img src="imagenes/editar.jpg" height="25" width="25"></td>
					<td><img src="imagenes/delete_icon.png" height="25" width="25"></td>

				</tr>
			</table>

		</form>
	</div>

</body>
</html>