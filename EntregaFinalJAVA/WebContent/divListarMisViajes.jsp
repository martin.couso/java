<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="estilos/estiloPrincipal.css">
<script type="text/javascript" src="scripts/animacionForms.js"></script>
<title></title>
</head>
<body>
	<div id="divRegistro" style="height: auto;">
		<form id="msform">
			<table class="divTablaComentarios">
				<tr>
					<th>Origen</th>
					<th>Destino</th>
					<th>Fecha</th>
					<th>Horario ida</th>
					<th>Horario vuelta</th>
					<th>Cantidad</th>
					<th>Estado</th>
					<th>Pasajeros</th>
					<th>Recorrido</th>

					<th>Eliminar</th>
				</tr>
				<tr>
					<td>7 y 48</td>
					<td>50 y 120</td>
					<td>24/10/2014</td>
					<td>14:00</td>
					<td>19:00</td>
					<td>4</td>
					<td>Pendiente</td>
					<td><a href="#verPasajeros"><img
							src="imagenes/pasajero.png" height="25" width="25"></a></td>
					<td><a href="#verRecorrido"><img
							src="imagenes/recorrido_icon.png" height="25" width="25"></a></td>
					<td><img src="imagenes/delete_icon.png" height="25" width="25"></td>

				</tr>
				<tr>
					<td>9 y 48</td>
					<td>50 y 120</td>
					<td>10/10/2014</td>
					<td>14:00</td>
					<td>17:00</td>
					<td>4</td>
					<td>Completado</td>
					<td><a href="#calificarPasajeros"><img
							src="imagenes/pasajero.png" height="25" width="25"></a></td>
					<td><a href="#verRecorrido"><img
							src="imagenes/recorrido_icon.png" height="25" width="25"></a></td>

					<td><img src="imagenes/delete_icon.png" height="25" width="25"></td>

				</tr>
			</table>

		</form>
		<div id="modificarRecorrido" class="modalDialog">
			<div>
				<a href="#close" title="Close" class="close">X</a>
				<h2>Modificar Recorrido</h2>
				<form id="msform">



					<input type="text" name="origen" value="7 y 48"
						placeholder="Origen" /> <input type="text" name="Destino"
						value="50 y 120" placeholder="Destino" /> <input type="date"
						name="fecha" value="24/10/2014" placeholder="Fecha  DD/MM/AAAA" />
					<input type="text" name="fecha" value="14:00"
						placeholder="Hora Ida" /> <input type="text" name="fecha"
						value="19:00" placeholder="Hora Vuelta" /> <input type="number"
						name="cantidad" value="4" placeholder="Cantidad de Pasajeros" />
					<img src="imagenes/recorrido.png"> <input type="submit"
						name="submit" class="submit action-button"
						value="Modificar Viaje!" />

				</form>

			</div>
		</div>

		<div id="verRecorrido" class="modalDialog">
			<div>
				<a href="#close" title="Close" class="close">X</a>
				<h2>Recorrido</h2>
				<form id="msform">

					<div id="mapaForm">
						<img src="imagenes/recorrido.png">
					</div>
					<input type="submit" name="submit" class="submit action-button"
						value="Alta Viaje!" />

				</form>

			</div>
		</div>
		<div id="verPasajeros" class="modalDialog">
			<div>
				<a href="#close" title="Close" class="close">X</a>
				<h2>Pasajeros</h2>
				<table class="divTablaComentarios">
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Calificacion</th>
						<th>Telefono</th>
						<th>rol</th>
					</tr>
					<tr>
						<td>juan</td>
						<td>xxxxx</td>
						<td>7.0</td>
						<td>4222222</td>
						<td>Conductor</td>
					</tr>
					<tr>
						<td>Pedro</td>
						<td>YYYY</td>
						<td>3.2</td>
						<td>4111111</td>
						<td>Acompaņante</td>

					</tr>
				</table>
			</div>
		</div>


		<div id="calificarPasajeros" class="modalDialog">
			<div>
				<a href="#close" title="Close" class="close">X</a>
				<h2>Califcar</h2>
				<table class="divTablaComentarios">
					<tr>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Calificacion</th>
						<th>Telefono</th>
						<th>Rol</th>
						<th>Denunciar</th>
					</tr>
					<tr>
						<td>juan</td>
						<td>xxxxx</td>
						<td><input min="0" max="10" type="number" /></td>
						<td>4222222</td>
						<td>conductor</td>
						<td><img src="imagenes/rechazar.png" height="25" width="25"></td>

					</tr>
				</table>
				
				<input onclick="#close" type="submit" name="aceptarCalificaciones" value="Confirmar">
			</div>
		</div>
	</div>
</body>
</html>