<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html >
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="estilos/estiloPrincipal.css">
<script type="text/javascript" src="scripts/animacionForms.js"></script>
<title>Insert title here</title>
</head>
<body>
	<div class="divControlViajeros" style="height: auto;">
		<!-- form  por pasos-->
		<form id="msform">

			<select name="criterioBusqueda">
				<option value="">Nombre</option>
				<option value="">Apellido</option>
				<option value="">Correo</option>
				<option value="">Calificaciones negativas mayores a</option>
				<option value="">Calificaciones postivas mayores a</option>
			</select> <input name="datoBuscado" type="text" placeholder="por ej: Juan"></input>

			<table class="divTablaComentarios">
				<tr>
					<th scope="col">Seleccion</th>
					<th scope="col">Nombre</th>
					<th scope="col">Apellido</th>
					<th scope="col">Correo</th>
					<th scope="col">Nickname</th>
					<th scope="col">Calificaciones Positivas</th>
					<th scope="col">Calificaciones Negativas</th>
					<th scope="col">Comentarios</th>
					<th scope="col">Viajes</th>
					<th scope="col">Bloquear Usuario</th>
					<th scope="col">Cancelar Viajes</th>
				</tr>

				<tr>
					<td><input type="checkbox"></td>
					<td>Martin</td>
					<td>Couso</td>
					<td>martin.couso@gmail.com</td>
					<td>martin_couso</td>
					<td>3</td>
					<td>53</td>
					<td>listaComentarios</td>
					<td>listaViajes</td>
					<td><a href="#"><img src="imagenes/delete_icon.png"
							alt="bloquear usuario" height="25" width="25"></a></td>
					<td><a href="#"><img src="imagenes/delete_icon.png"
							alt="cancelar viajes" height="25" width="25"></a></td>
				</tr>
				<tr>
					<td><input type="checkbox"></td>
					<td>Lisandro</td>
					<td>Direnta</td>
					<td>lisandro@gmail.com</td>
					<td>lisandro_D</td>
					<td>5</td>
					<td>50</td>
					<td>listaComentarios</td>
					<td>listaViajes</td>
					<td><a href="#"><img src="imagenes/delete_icon.png"
							alt="bloquear usuario" height="25" width="25"></a></td>
					<td><a href="#"><img src="imagenes/delete_icon.png"
							alt="cancelar viajes" height="25" width="25"></a></td>
				</tr>

			</table>
			<input type="submit" name="submit" class="submit action-button"
				value="Confirmar" />

		</form>

		<!-- jQuery -->
		<script src="http://thecodeplayer.com/uploads/js/jquery-1.9.1.min.js"
			type="text/javascript"></script>
		<!-- jQuery easing plugin -->
		<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js"
			type="text/javascript"></script>

		<script src="scripts/animacionForms.js"></script>
	</div>
</body>
</html>