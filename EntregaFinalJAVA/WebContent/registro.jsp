<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css"
	href="estilos/estiloPrincipal.css">
  <script type="text/javascript" src="scripts/animacionForms.js"></script>
  <link rel="shortcut icon" href="favicon.ico" />
<title>Registro</title>
</head>
<body>


	<%@include file="header.jsp"%>
<div id="divRegistro">	
	<!-- form  por pasos-->
	<form id="msform">
		<!-- progressbar -->
		<ul id="progressbar">
			<li class="active">Datos de la cuenta</li>
			<li>Datos Sociales</li>
			<li>Datos Personales</li>
		</ul>
		<!-- fieldsets -->
		<fieldset>
			<h2 class="fs-title">Crea tu cuenta!</h2>
			<h3 class="fs-subtitle">Comenzamos?</h3>
			<input type="text" name="email" placeholder="Tu correo" /> <input
				type="password" name="pass" placeholder="Una clave" /> <input
				type="password" name="cpass" placeholder="Confirma la clave" /> <input
				type="button" name="next" class="next action-button" value="Siguiente" />
		</fieldset>
		<fieldset>
			<h2 class="fs-title">Datos Sociales</h2>
			<h3 class="fs-subtitle">Casi terminamos</h3>
			<input type="text" name="twitter" placeholder="Twitter" /> <input
				type="text" name="facebook" placeholder="Facebook" /> <input
				type="text" name="gplus" placeholder="Google Plus" /> <input
				type="button" name="previous" class="previous action-button"
				value="Anterior" /> <input type="button" name="next"
				class="next action-button" value="Siguiente" />
		</fieldset>
		<fieldset>
			<h2 class="fs-title">Datos Personales</h2>
			<h3 class="fs-subtitle">Listo!</h3>
			<input type="text" name="nombre" placeholder="Tu nombre" /> <input
				type="text" name="apellido" placeholder="Tu apellido" /> <input
				type="text" name="telefono" placeholder="Tu n�mero telef�nico" />
			<textarea name="direccion" placeholder="Tu direcci�n actual"></textarea>
			<input type="button" name="previous" class="previous action-button"
				value="Anterior" /> <input type="submit" name="submit"
				class="submit action-button" value="Registrarme!" />
		</fieldset>
	</form>

	<!-- jQuery -->
	<script src="http://thecodeplayer.com/uploads/js/jquery-1.9.1.min.js"
		type="text/javascript"></script>
	<!-- jQuery easing plugin -->
	<script src="http://thecodeplayer.com/uploads/js/jquery.easing.min.js"
		type="text/javascript"></script>

		 <script src="scripts/animacionForms.js"></script>
</div>
</body>
</html>