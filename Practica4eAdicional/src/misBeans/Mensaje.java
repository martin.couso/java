package misBeans;

import java.io.Serializable;

public class Mensaje  implements Serializable{

	/*------------------------------ Comienza Atributos ------------------------------------------*/	
	private String mensaje;
	private String publicador;
	
	public Mensaje(String mensaje, String publicador) {
		super();
		this.mensaje = mensaje;
		this.publicador = publicador;
	}
	
	/*------------------------------ Finaliza Atributos ------------------------------------------*/
	
	
	
	/*------------------------------ Comienza Comportamiento -------------------------------------*/
	
	public Mensaje() {
		super();
	}

	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getPublicador() {
		return publicador;
	}
	public void setPublicador(String publicador) {
		this.publicador = publicador;
	}

	/*------------------------------ Finaliza Comportamiento -------------------------------------*/
	
}
