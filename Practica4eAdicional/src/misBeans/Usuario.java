package misBeans;

public class Usuario {

	/*------------------------------ Comienza Atributos ------------------------------------------*/

	private String nombre;
	private String password;
	private boolean activo; 
	private String[] areas;
	private String dom;

	/*------------------------------ Finaliza Atributos ------------------------------------------*/

	
	/*------------------------------ Comienza Comportamiento -------------------------------------*/
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isActivo() {
		return activo;
	}
	public void setActivo(boolean activo) {
		this.activo = activo;
	}
	public String[] getAreas() {
		return areas;
	}
	public void setAreas(String[] areas) {
		this.areas = areas;
	}
	public String getDom() {
		return dom;
	}
	public void setDom(String dom) {
		this.dom = dom;
	}
	
	/*------------------------------ Finaliza Comportamiento -------------------------------------*/


}
