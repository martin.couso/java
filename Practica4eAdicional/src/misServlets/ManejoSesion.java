package misServlets;

import java.io.IOException;
import java.util.ArrayList;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import misBeans.Usuario;

/**
 * Servlet implementation class ManejoSesion
 */
public class ManejoSesion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ManejoSesion() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		//Se recuperan los datos enviandos en el request desde login.jsp
		Usuario usuarioLogueado = new Usuario();
		String nombreUsuario;
		nombreUsuario = request.getParameter("nombre");
		String passUsuario= request.getParameter("clave");
		Boolean encontre = false;
	

		
		//Se recupera la lista de usuarios desde el contexto 		
		ArrayList<Usuario> unaListaDeUsuarios = (ArrayList<Usuario>) request.getServletContext().getAttribute("listaUsuarios");
		//Se recorre la lista en busca de los datos ingresados
		for (Usuario unUsuario : unaListaDeUsuarios) {
			//si es un usuario v�lido la varible de sesi�n se settea en true al igual que la variable de confirmaci�n "encontre"
			if ((unUsuario.getNombre().equals(nombreUsuario) )&&(unUsuario.getPassword().equals(passUsuario))){
				encontre=true;
				request.getSession().setAttribute("logueado", "true");
				request.getSession().setAttribute("user", nombreUsuario);
			}
		}
		//Si los datos no fueron encontrados se redirecciona al cliente al Index.html
		if (encontre ==false) {
			RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/login.jsp");
			dispatcher.forward(request, response);
		}else {
			request.setAttribute("objetoUsuario",usuarioLogueado);
			RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/Index.html");
			dispatcher.forward(request, response);
		}
	}

}
