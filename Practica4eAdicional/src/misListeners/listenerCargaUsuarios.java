package misListeners;

import java.util.ArrayList;

import javax.servlet.ServletContext;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


import misBeans.*;

/**
 * Application Lifecycle Listener implementation class listenerCargaUsuarios
 *
 */
public class listenerCargaUsuarios implements ServletContextListener {

  

    // Carga los usuarios habilitados para usar el sistema al contexto de aplicación asi como una lista vacia de mensajes
    public void contextInitialized(ServletContextEvent evento)  { 
    	
    	try{
    	ServletContext context = evento.getServletContext();
    	
    	Usuario usuario1 = new Usuario();
    	usuario1.setNombre("Administrador");
    	usuario1.setPassword("Administrador");
    	
    	Usuario usuario2 = new Usuario();
    	usuario2.setNombre("Gestor");
    	usuario2.setPassword("Gestor");
    	
    	Usuario usuario3 = new Usuario();
    	usuario3.setNombre("Consultador");
    	usuario3.setPassword("Consultador");
    	
    	ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();    	
    	listaUsuarios.add(usuario1);
    	listaUsuarios.add(usuario2);
    	listaUsuarios.add(usuario3);  
    	
    	ArrayList<Mensaje> unaLista = new ArrayList<Mensaje>();
    	context.setAttribute("listaUsuarios", listaUsuarios);
    	context.setAttribute("listaMensajes", unaLista);
    	
    	}catch(Exception e){
    		System.out.print("Hubo errores al cargar los usuarios y la lista de mensajes");
    	}
    }

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	
}
