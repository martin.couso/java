package filtros;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class FiltrosDeLogs
 */
public class FiltrosDeLogs implements Filter {
	private FilterConfig config;

    /**
     * Default constructor. 
     */
    public FiltrosDeLogs() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		
		//ip
		String ip = request.getRemoteAddr();
	//ip real
		System.out.println(InetAddress.getLocalHost().getHostAddress());
	//nombre de mi host
		System.out.println(InetAddress.getLocalHost().getHostName());
	//ni idea
//		System.out.println(ip);

		 //Instanciamos el objeto Calendar
        //en fecha obtenemos la fecha y hora del sistema
        Calendar fecha = new GregorianCalendar();
        //Obtenemos el valor del a�o, mes, d�a,
        //hora, minuto y segundo del sistema
        //usando el m�todo get y el par�metro correspondiente
        int a�o = fecha.get(Calendar.YEAR);
        int mes = fecha.get(Calendar.MONTH);
        int dia = fecha.get(Calendar.DAY_OF_MONTH);
        int hora = fecha.get(Calendar.HOUR_OF_DAY);
        int minuto = fecha.get(Calendar.MINUTE);
        int segundo = fecha.get(Calendar.SECOND);
        System.out.println(dia + "/" + (mes+1) + "/" + a�o +" "+  hora+":"+  minuto+":"+ segundo);
		
		
    //    String refererURI = new URI(request.getHeader("referer")).getPath();

    	HttpServletRequest req = (HttpServletRequest) request;
	
    	System.out.println(req.getHeader( "user-agent"));
    	System.out.println(req.getHeader("Accept-Language"));
    	System.out.println(req.getHeader("host"));
		System.out.println(req.getHeader("X-Forwarded-For"));
		System.out.println(req.getHeader("referer"));
		chain.doFilter(request, response);
		
	
		
		
		
      
		
	
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		this.config = config;

	}

}
