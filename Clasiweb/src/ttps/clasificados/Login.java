package ttps.clasificados;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.If;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
	
	private ArrayList<Usuario> listaUsuarios;
    public Login() {
        // TODO Auto-generated constructor stub
    }

	/*
	 * @see Servlet#init(ServletConfig)
	 */
    
    public void init(ServletConfig config) throws ServletException {
    	super.init(config);
    	listaUsuarios = new ArrayList<Usuario>();
    	Usuario usuario1 =  new Usuario();
    	Usuario usuario2 = new Usuario();
    	usuario1.setClave("admin");
    	usuario1.setUsuario("admin");
    	usuario1.setPerfil("Administrador");
    	usuario2.setClave("publicador");
    	usuario2.setUsuario("publicador");
    	usuario2.setPerfil("Publicador");
    	listaUsuarios.add(usuario1);
    	listaUsuarios.add(usuario2);
    	
    	
    	
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		Usuario usuarioLogueado = new Usuario();
		String nombreUsuario;
		nombreUsuario = request.getParameter("user");
		String passUsuario= request.getParameter("pass");
		Boolean encontre = false;
		for (Usuario usuario : listaUsuarios) {
			if ((usuario.getUsuario().equals(nombreUsuario)) && (usuario.getClave().equals(passUsuario) )) {
				encontre=true;
				usuarioLogueado=usuario;
				
			}
		}
		if (encontre ==false) {
			response.sendRedirect("www.google.com");
		}else {
			request.setAttribute("objetoUsuario",usuarioLogueado);
			RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/Menu");
			dispatcher.forward(request, response);
		}
		
		
		
		/*
		String valor = listaUsuarios.get(nombre);
		if (pass.equals( listaUsuarios.get(nombre)) ) {
			PrintWriter out = response.getWriter();
			out.println("<html><body>");
			out.println("<h1> Hola   </h1>");
			out.print(" </body></html>");
			out.close();
		}  else {
			
		}*/
		
	}

}
