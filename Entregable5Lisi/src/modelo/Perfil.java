package modelo;
import javax.persistence.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//resubido con anotaciones
@Entity
@Table(name = "perfil")
public class Perfil {
	@Id
	@Column(name = "id")
	@GeneratedValue()
	Integer id;
	@Column(name = "tipo", nullable = false)
	String tipo;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	
	
	

}
