package modelo;
import javax.persistence.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

//resubido con anotaciones
@Entity
@Table(name = "usuario")
public class Usuario {
	@Id
	@Column(name = "id")
	@GeneratedValue()
	Integer id;
	@Column(name = "nombre", nullable = false)
	String nombre;
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	Perfil perfil;
	
	
/*	String apellido;
	String usuario;
	String contrasena;
	String mail;
	String telefono;
	Integer estado;*/
	
	
	public Integer getId() {
		return id;
	}
	public Perfil getPerfil() {
		return perfil;
	}
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	

}
