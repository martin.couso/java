<%@page import="java.util.ArrayList"%>
<%@page import="misBeans.Mensaje"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="estiloPrincipal.css">
<title>Lista de mensajes</title>
</head>
<body>
	<%@include file="header.jsp"%>

	<%
		ArrayList<Mensaje> listaRecuperada = (ArrayList<Mensaje>) application
				.getAttribute("listaMensajes");
		Mensaje msj1 = new Mensaje("comentario1", "Autor1");
		listaRecuperada.add(msj1);
	%>
	<div class="divTablaComentarios">
		<table>
			<tr>
				
				<th>Mensaje</th>
				<th>:Autor</th>
			</tr>

			<c:forEach items="${listaMensajes}" var="mensajes">
				<tr>
				
					<td><c:out value="${mensajes.mensaje}" /></td>
					<td><c:out value=":${mensajes.publicador}" /></td>
					
				</tr>
			</c:forEach>
		</table>

	</div>

	

</body>
</html>