<%@page import="java.util.ArrayList"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" type="text/css" href="estiloPrincipal.css">
<title>Comprar Entradas</title>
</head>
<body>

	<%@include file="header.jsp"%>

	<%
			ArrayList<String> listaLugaresVenta = (ArrayList<String>) application.getAttribute("listaPuntosDeVenta");
			%>

	<form action="ChequeoCompra" method="Post">
		<fieldset>
			<label>Tarjeta de cr&eacutedito:</label>
			<select name="selectTarjetas">
				<option value="Visa">Visa</option>
				<option value="MC">Master Card</option>
				<option value="AE">American Express</option>
			</select> 
			<br>
			<br>
			<label>N&uacutemero de tarjeta:</label>
			<input type="text" name="numeroTarjeta" placeHolder="ingrese n&mero de tarjeta">
			<br>
			<br>
			<label>Cantidad de entradas:</label>
			<input type="number" min="0" max="100" name="cantEntradas" />
			<br>
			<br>
			<label>C&oacutedigo de seguridad:</label>
			<input type="text" name="codigoSeguridad" placeHolder="ingrese codigo de seguridad">
			<br>
			<br>
			<label>Lugar de retiro de entradas:</label>
			<select name="lugares">
				<c:forEach items="${listaPuntosDeVenta}" var="lugar">
					<option value="${lugar}">${lugar}</option>
				</c:forEach>
			</select> 
			<br>
			<br>
			<input type="submit" value="Comprar!">
		</fieldset>
	</form>


	<%@include file="footer.jsp"%>



</body>
</html>