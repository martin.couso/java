package misServlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import misBeans.Usuario;

/**
 * Servlet implementation class ChequeoCompra
 */
@WebServlet("/ChequeoCompra")
public class ChequeoCompra extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ChequeoCompra() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		//Se recuperan los datos enviandos en el request desde login.jsp
		
		String selectTarjetas = request.getParameter("selectTarjetas");
		String numeroTarjeta = request.getParameter("numeroTarjeta");
		String cantEntradas = request.getParameter("cantEntradas");
		String codigoSeguridad = request.getParameter("codigoSeguridad");
		String lugares = request.getParameter("lugares");
		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/resultadoCompra.jsp");
		dispatcher.forward(request, response);
		
	}

}
