package misBeans;

public class Usuario {

	/*------------------------------ Comienza Atributos ------------------------------------------*/

	private String nombre;
	private String password;
	private String apellido;

	/*------------------------------ Finaliza Atributos ------------------------------------------*/



	/*------------------------------ Comienza Comportamiento -------------------------------------*/
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}



	/*------------------------------ Finaliza Comportamiento -------------------------------------*/


}
