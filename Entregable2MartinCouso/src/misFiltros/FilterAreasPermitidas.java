package misFiltros;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class FilterAreasPermitidas
 */
@WebFilter({ "/FilterAreasPermitidas", "/comprar.jsp", "/formuMensaje.jsp" })
public class FilterAreasPermitidas implements Filter {

    /**
     * Default constructor. 
     */
    public FilterAreasPermitidas() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		//Castea los request  y response a HTTPSERVLET porque el filtro puede captar cualquier tipo
		HttpServletRequest req = (HttpServletRequest)request;
		HttpServletResponse res = (HttpServletResponse) response;
		//Verifica que el usuario est� logueado cuando intenta acceder a publicar un nuevo mensaje, sino lo esta lo redirecciona a login.jsp
		if ((req.getSession().getAttribute("logueado")==null)) {
			res.sendRedirect("login.jsp");
		}else{
			chain.doFilter(req, res);
		}

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
