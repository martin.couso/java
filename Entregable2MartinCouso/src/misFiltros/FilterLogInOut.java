package misFiltros;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class FilterLogInOut
 */
@WebFilter({ "/FilterLogInOut", "/login.jsp" })
public class FilterLogInOut implements Filter {

    /**
     * Default constructor. 
     */
    public FilterLogInOut() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		//Castea los request  y response a HTTPSERVLET porque el filtro puede captar cualquier tipo
				HttpServletRequest req = (HttpServletRequest)request;
				HttpServletResponse res = (HttpServletResponse) response;
				
				//Seg�n est� logueado o no cuando el usuario intenta acceder a login.jsp se le permite el acceso o se le redirecciona a salir.jsp
				if (!(req.getSession().getAttribute("logueado")==null)) {
					res.sendRedirect("salir.jsp");
				}else{
					chain.doFilter(req, res);
				}
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
