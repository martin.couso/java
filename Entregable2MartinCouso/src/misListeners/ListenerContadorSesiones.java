package misListeners;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class ListenerContadorSesiones
 *
 */
@WebListener
public class ListenerContadorSesiones implements HttpSessionListener {
	
	static private int sesionesActivas;


	public static int getSesionesActivas() { 
		return sesionesActivas; 
	}




	/**
	 * Default constructor. 
	 */
	public ListenerContadorSesiones() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
	 */
	public void sessionCreated(HttpSessionEvent arg0)  { 
		sesionesActivas++;

	}

	/**
	 * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
	 */
	public void sessionDestroyed(HttpSessionEvent arg0)  { 
		sesionesActivas--;

	}

}
