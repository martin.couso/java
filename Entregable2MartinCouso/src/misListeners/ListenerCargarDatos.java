package misListeners;

import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;



import misBeans.Mensaje;
import misBeans.Usuario;

/**
 * Application Lifecycle Listener implementation class ListenerCargarDatos
 *
 */
@WebListener
public class ListenerCargarDatos implements ServletContextListener {

    /**
     * Default constructor. 
     */
    public ListenerCargarDatos() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0)  { 
         // TODO Auto-generated method stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent evento)  { 
    	try{
        	ServletContext context = evento.getServletContext();
        	
        	Usuario usuario1 = new Usuario();
        	usuario1.setNombre("punk");
        	usuario1.setPassword("punk");
        	usuario1.setApellido("Suarez");
        	
        	Usuario usuario2 = new Usuario();
        	usuario2.setNombre("admin");
        	usuario2.setPassword("admin");
        	usuario2.setApellido("admin");
        	
        	Usuario usuario3 = new Usuario();
        	usuario3.setNombre("rockero");
        	usuario3.setPassword("rockero");
        	usuario3.setApellido("Bolivar");
        	
        	ArrayList<Usuario> listaUsuarios = new ArrayList<Usuario>();    	
        	listaUsuarios.add(usuario1);
        	listaUsuarios.add(usuario2);
        	listaUsuarios.add(usuario3);  
        	
        	
        	
        	ArrayList<String> listaPuntosDeVenta = new ArrayList<String>();
        	listaPuntosDeVenta.add("Microcentro-Viamonte 560, locales 6 y 8, CApital Federal .Lunes a Viernes de 12 a 19hs.");
        	listaPuntosDeVenta.add("Abasto - Av Corrientes 3200, Capital FEderal. 1er piso. Todos los d�as de 10 a 22hs.");
        	listaPuntosDeVenta.add("Jason Rock - Calle 6 N� 817(entre 48 y 49).La Plata, Bs As.Lunes a S�bados de 12 a 20hs.");
        	listaPuntosDeVenta.add("Palermo - Av. Santa Fe 4389, Capital Federal. Lunes a S�bados de 12 a 20hs.");
        					
        	ArrayList<Mensaje> listaMensajes = new ArrayList<Mensaje>();
        	context.setAttribute("listaMensajes", listaMensajes);
           	context.setAttribute("listaUsuarios", listaUsuarios);
        	context.setAttribute("listaPuntosDeVenta", listaPuntosDeVenta);
        	
        	}catch(Exception e){
        		System.out.print("Hubo errores al cargar los usuarios, los mensajes y/o los puntos de venta -->" + e);
        	}
    }
	
}
